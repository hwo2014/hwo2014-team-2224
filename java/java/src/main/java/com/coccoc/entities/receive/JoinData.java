package com.coccoc.entities.receive;

public class JoinData {
	public Data data;
	public String msgType;

	public static class Data {
		public String name;
		public String key;
	}
}

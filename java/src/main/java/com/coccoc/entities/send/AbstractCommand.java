package com.coccoc.entities.send;

import com.coccoc.MsgWrapper;
import com.google.gson.Gson;

public abstract class AbstractCommand {
	public String toJson() {
		return new Gson().toJson(new MsgWrapper(this));
	}

	public Object msgData() {
		return this;
	}

	public abstract String msgType();
}

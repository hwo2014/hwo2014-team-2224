package com.coccoc;

import java.util.Arrays;

public class GameState {

	public static final int NEXT_TURN_LEFT = 0;
	public static final int NEXT_TURN_RIGHT = 1;
	public int nLaps; // 0 means it is qualification round (use this chance to detect possible failure (while there is no opponents).
	public int nLanes;
	public int nPieces;
	public boolean[] isStraightPieces;
	public boolean[] isSwitchablePieces;
	//lanes x lengths
	public double[][] piecesLengths;
	public double[] piecesAngles;
	public double[] piecesRadiuses;
	public double[] distanceToNextBends;
	public int[] nextTurns; // 0-Left, 1-Right, -1-NA
	public int[] nextBendId;
	public boolean hasBendPieces; // I think it must always be FALSE, otherwise how you can go back to the start/finish line :D
	public boolean isQualificationRound;
	public boolean[] shouldTurbos;

	public boolean isStraight(int pid) {
		return isStraightPieces[pid];
	}

	public int getNextPieceIndex(int curr, int delta) {
		return (curr + delta + nPieces) % nPieces;
	}

	public int getNextPieceIndex(int curr) {
		return getNextPieceIndex(curr, 1);
	}

	public int getLastPieceIndex(int curr) {
		return getNextPieceIndex(curr, -1);
	}

	@Override
	public String toString() {
		String str = "***GameState\n: ";
		str += "\t nLaps = " + nLaps + "\n";
		str += "\t nLanes = " + nLanes + "\n";
		str += "\t nPieces = " + nPieces + "\n";
		str += "\t isStraightPieces = " + Arrays.toString(isStraightPieces) + "\n";
		str += "\t isSwitchablePieces = " + Arrays.toString(isSwitchablePieces) + "\n";
		for (int i = 0; i < nLanes; ++i) {
			str += "\t piecesLengths[" + i + "] = " + Arrays.toString(piecesLengths[i]) + "\n";
		}
		str += "\t piecesAngles = " + Arrays.toString(piecesAngles) + "\n";
		str += "\t piecesRadiuses = " + Arrays.toString(piecesRadiuses) + "\n";
		str += "\t distanceToNextBends = " + Arrays.toString(distanceToNextBends) + "\n";
		str += "\t nextTurns = " + Arrays.toString(nextTurns) + "\n";
		str += "\t hasBendPieces = " + hasBendPieces + "\n";
		str += "\t isQualificationRound = " + isQualificationRound + "\n";

		return str;
	}

}

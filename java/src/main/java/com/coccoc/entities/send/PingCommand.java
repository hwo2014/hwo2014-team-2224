package com.coccoc.entities.send;

public class PingCommand extends AbstractCommand {
	@Override
	public String msgType() {
		return "ping";
	}
}

package com.coccoc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;

import com.coccoc.entities.receive.CarPositionsData;
import com.coccoc.entities.receive.GameInitData;
import com.coccoc.entities.send.AbstractCommand;
import com.coccoc.entities.send.JoinCommand;
import com.coccoc.entities.send.PingCommand;
import com.coccoc.entities.send.ThrottleCommand;
import com.google.gson.Gson;

/**
 * This is a demo version. Stop improve it.
 *
 * @author Coc Coc developers
 */
public class RobotNoob {
	public static void main(String... args) throws IOException {
		String host = "webber.helloworldopen.com";
		int port = 8091;
		String botName = "Coc Coc";
		String botKey = "o8FWGE73+p5P1w";

		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		new RobotNoob(reader, writer, new JoinCommand(botName, botKey));
	}

	private static final Gson GSON = new Gson();
	private final PrintWriter writer;

	private GameInitData gameInitData;
	private CarPositionsData carPositionsData;

	public RobotNoob(final BufferedReader reader, final PrintWriter writer, final JoinCommand join) throws IOException {
		this.writer = writer;
		String line = null;

		send(join);

		while ((line = reader.readLine()) != null) {
			System.out.println(line);
			final MsgWrapper msgFromServer = GSON.fromJson(line, MsgWrapper.class);
			if (msgFromServer.msgType.equals("carPositions")) {
				carPositionsData = GSON.fromJson(line, CarPositionsData.class);
				makeDecision();
			} else if (msgFromServer.msgType.equals("join")) {
				System.out.println("Joined");
			} else if (msgFromServer.msgType.equals("gameInit")) {
				System.out.println("\n====== RACE INIT ======");
				gameInitData = GSON.fromJson(line, GameInitData.class);
				initData();
			} else if (msgFromServer.msgType.equals("gameEnd")) {
				System.out.println("\n====== RACE END ======");
			} else if (msgFromServer.msgType.equals("gameStart")) {
				System.out.println("\n====== RACE START ======");
			} else {
				send(new PingCommand());
			}
		}
	}

	private void makeDecision() {
		int currPieceIndex = carPositionsData.data[0].piecePosition.pieceIndex;
		double inPieceDis = carPositionsData.data[0].piecePosition.inPieceDistance;
		int nextPieceIndex = (currPieceIndex + 1) % nPieces;
		double nextCurve = nextCurve(currPieceIndex, inPieceDis);
		double speed = 1.0;
		if (hasCurvePiece) {
			if (nextCurve > 30) {
				speed = 1.0;
			} else if (nextCurve > 27) {
				speed = 0.0;
			} else {
				speed = 0.5;
			}
		}
		send(new ThrottleCommand(speed));
	}

	// FIXME: can be O(1)
	private double nextCurve(int currPiece, double currDistance) {
		if (!hasCurvePiece) {
			return -1;
		}
		if (isStraightPieces[currPiece]) {
			return 0;
		}
		double distance = piecesLengths[currPiece] - currDistance;
		currPiece = (currPiece + 1) % nPieces;
		while (isStraightPieces[currPiece]) {
			distance += piecesLengths[currPiece];
			currPiece = (currPiece + 1) % nPieces;
		}
		return distance;
	}

	private int nLaps; // 0 means it is qualification round
	private int nLanes;
	private int nPieces;
	private boolean[] isStraightPieces;
	private boolean[] isSwitchablePieces;
	private double[] piecesLengths;
	private double[] piecesAngles;
	private double[] piecesRadiuses;

	private boolean hasCurvePiece;
	CarState me = new CarState();

	private void initData() {
		nLaps = (gameInitData.data.race.raceSession.durationMs != null) ? 0 : gameInitData.data.race.raceSession.laps;
		nLanes = gameInitData.data.race.track.lanes.size();
		nPieces = gameInitData.data.race.track.pieces.size();
		isStraightPieces = new boolean[nPieces];
		isSwitchablePieces = new boolean[nPieces];
		piecesAngles = new double[nPieces];
		piecesRadiuses = new double[nPieces];
		piecesLengths = new double[nPieces];
		hasCurvePiece = false;
		for (int i = 0; i < nPieces; i++) {
			Map<String, Object> p = gameInitData.data.race.track.pieces.get(i);
			isStraightPieces[i] = p.containsKey("length");
			hasCurvePiece |= !isStraightPieces[i];
			isSwitchablePieces[i] = p.containsKey("switch") && Boolean.TRUE.equals(p.get("switch"));
			piecesLengths[i] = isStraightPieces[i] ? Double.parseDouble(p.get("length").toString()) : -1;
			piecesAngles[i] = isStraightPieces[i] ? -1 : Double.parseDouble(p.get("angle").toString());
			piecesRadiuses[i] = isStraightPieces[i] ? -1 : Double.parseDouble(p.get("radius").toString());
		}
		me.radius = piecesRadiuses[0];
	}

	private void send(final AbstractCommand command) {
		writer.println(command.toJson());
		writer.flush();
	}
}

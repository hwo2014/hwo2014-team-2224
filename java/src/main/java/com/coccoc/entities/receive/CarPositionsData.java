package com.coccoc.entities.receive;


public class CarPositionsData {
	public String gameId;
	public DataEntry[] data;
	public String msgType;

	public static class DataEntry {
		public CarData id;
		public PiecePosition piecePosition;
		public Double angle;
	}

	public static class PiecePosition {
		public Double inPieceDistance;
		public Integer lap;
		public Integer pieceIndex;
		public Lane lane;
	}

	public static class Lane {
		public Integer startLaneIndex;
		public Integer endLaneIndex;
	}
}

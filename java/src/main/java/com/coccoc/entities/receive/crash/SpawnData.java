package com.coccoc.entities.receive.crash;

import com.coccoc.entities.receive.CarData;

public class SpawnData {
	public String gameId;
	public CarData data;
	public Integer gameTick;
	public String msgType;
}

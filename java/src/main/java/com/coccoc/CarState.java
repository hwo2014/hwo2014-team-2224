package com.coccoc;

import com.coccoc.entities.receive.turbo.TurboAvailableData;

public class CarState {
	double throttle = 1.0;

	// per tick velocity
	double velocity = Helper.DOUBLE_ZERO;
	double acceleration = Helper.DOUBLE_ZERO;
	double carAngle = Helper.DOUBLE_ZERO;
	double radius = Helper.DOUBLE_ZERO;

	int pieceIndex = Helper.INTEGER_NA;
	double inPieceDistance = Helper.DOUBLE_NA;
	double distanceToBend = Helper.DOUBLE_NA;
	int laneIndex = Helper.INTEGER_NA;
	boolean turbo = false;
	TurboAvailableData turboAvail = null;
	boolean turboSending = false;
	TurboAvailableData actualTurbo = null;

	int switchingPieceIndex = -1; // usually is -1, set to the piece of last time sending switching lane command

	public static String toString(CarState prev, CarState curr) {
		if (prev == null) {
			prev = new CarState();
		}
		if (curr == null) {
			curr = new CarState();
		}
		return String.format("throttle: %.2f -> %.2f\tvelocity: %.2f - > %.2f\tacc: %.2f -> %.2f\tangle: %.2f -> %.2f\tradius: %.2f -> %.2f\t"
				+ "pieceId: %d -> %d\tinPieceDis: %.2f -> %.2f\tlane: %d -> %d\tturbo: %b -> %b\tdistanceToBend: %.2f -> %.2f",
				prev.throttle, curr.throttle, prev.velocity, curr.velocity, prev.acceleration, curr.acceleration, prev.carAngle, curr.carAngle, prev.radius, curr.radius,
				prev.pieceIndex, curr.pieceIndex, prev.inPieceDistance, curr.inPieceDistance, prev.laneIndex, curr.laneIndex, prev.turbo, curr.turbo, prev.distanceToBend, curr.distanceToBend);
	}

	public static void copy(CarState source, CarState target) {
		target.throttle = source.throttle;
		target.velocity = source.velocity;
		target.acceleration = source.acceleration;
		target.carAngle = source.carAngle;
		target.radius = source.radius;
		target.pieceIndex = source.pieceIndex;
		target.inPieceDistance = source.inPieceDistance;
		target.laneIndex = source.laneIndex;
		target.turbo = source.turbo;
		target.switchingPieceIndex = source.switchingPieceIndex;
		target.turboAvail = source.turboAvail;
		target.actualTurbo = source.actualTurbo;
		target.turboSending = source.turboSending;
		target.distanceToBend = source.distanceToBend;
	}
}

package com.coccoc.entities.send;

public class TurboCommand extends AbstractCommand {

	public static final TurboCommand INSTANCE = new TurboCommand();

	public final String data = "Coc Coc to da moon...";

	@Override
	public Object msgData() {
		return data;
	}

	@Override
	public String msgType() {
		return "turbo";
	}
}

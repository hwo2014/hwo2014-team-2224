package com.coccoc.entities.send;

public class JoinCommand extends AbstractCommand {
	public final String name;
	public final String key;

	public JoinCommand(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	public String msgType() {
		return "join";
	}
}

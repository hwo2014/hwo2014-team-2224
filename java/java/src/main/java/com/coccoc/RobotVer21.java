package com.coccoc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.coccoc.entities.receive.CarPositionsData;
import com.coccoc.entities.receive.GameInitData;
import com.coccoc.entities.receive.YourCarData;
import com.coccoc.entities.receive.turbo.TurboAvailableData;
import com.coccoc.entities.receive.turbo.TurboEndData;
import com.coccoc.entities.receive.turbo.TurboStartData;
import com.coccoc.entities.send.AbstractCommand;
import com.coccoc.entities.send.JoinCommand;
import com.coccoc.entities.send.PingCommand;
import com.coccoc.entities.send.SwitchLaneCommand;
import com.coccoc.entities.send.ThrottleCommand;
import com.coccoc.entities.send.TurboCommand;
import com.google.gson.Gson;
import java.util.Arrays;

/**
 * The 2nd version. Simple turbo. Try other veloc tatic
 *
 * @author Coc Coc developers
 */
public class RobotVer21 {

	private static final Gson GSON = new Gson();
	private final PrintWriter writer;

	private static final double SAFETY_DISTANCE_FULL_SPEED = 300;
	private static final double DISTANCE_TO_REDUCE_SPEED = 250;

	private GameInitData gameInitData;
	private CarPositionsData carPositionsData;

	private final List<CarState> opponentStates;
	private final CarState prevCarState;
	private final CarState currCarState;
	private final GameState gameState;
	private YourCarData yourCar;

	public RobotVer21(final BufferedReader reader, final PrintWriter writer, final JoinCommand join) throws IOException {
		this.writer = writer;
		String line = null;

		send(join);

		opponentStates = new ArrayList<CarState>();
		prevCarState = new CarState();
		currCarState = new CarState();
		gameState = new GameState();

		while ((line = reader.readLine()) != null) {
			System.out.println("\nRECV: " + line);
			final MsgWrapper msgFromServer = GSON.fromJson(line, MsgWrapper.class);
			if (msgFromServer.msgType.equals("carPositions")) {
				carPositionsData = GSON.fromJson(line, CarPositionsData.class);
				Helper.calculateCarState(carPositionsData, gameState, yourCar, opponentStates, prevCarState, currCarState);
				makeDecision();
				System.out.println("STATE == " + CarState.toString(prevCarState, currCarState));
			} else if (msgFromServer.msgType.equals("join")) {
				System.out.println("\n====== JOINED ======");
			} else if (msgFromServer.msgType.equals("gameInit")) {
				System.out.println("\n====== RACE INIT ======");
				gameInitData = GSON.fromJson(line, GameInitData.class);
				Helper.initData(gameInitData, gameState, currCarState);
				currCarState.pieceIndex = 0;
				prevCarState.pieceIndex = gameState.nPieces - 1;
			} else if (msgFromServer.msgType.equals("turboAvailable")) {
				System.out.println("\n====== TURBO AVAILABLE ======");
				currCarState.turboAvail = GSON.fromJson(line, TurboAvailableData.class);
			} else if (msgFromServer.msgType.equals("gameEnd")) {
				System.out.println("\n====== RACE END ======");
			} else if (msgFromServer.msgType.equals("yourCar")) {
				System.out.println("\n====== YOUR CAR ======");
				yourCar = GSON.fromJson(line, YourCarData.class);
			} else if (msgFromServer.msgType.equals("gameStart")) {
				System.out.println("\n====== RACE START ======");
			} else if (msgFromServer.msgType.equals("turboStart")) {
				System.out.println("\n====== TURBO START ======");
				TurboStartData turbo = GSON.fromJson(line, TurboStartData.class);
				if (yourCar.data.equals(turbo.data)) {
					currCarState.actualTurbo = currCarState.turboAvail;
					currCarState.turboAvail = null;
					currCarState.turbo = true;
					currCarState.turboSending = prevCarState.turboSending = false;
				}
			} else if (msgFromServer.msgType.equals("turboEnd")) {
				System.out.println("\n====== TURBO END ======");
				TurboEndData turbo = GSON.fromJson(line, TurboEndData.class);
				if (yourCar.data.equals(turbo.data)) {
					currCarState.actualTurbo = null;
					currCarState.turbo = false;
				}
			} else if (msgFromServer.msgType.equals("gameStart")) {
				System.out.println("\n====== RACE START ======");
			} else if (msgFromServer.msgType.equals("crash")) {
				System.out.println("\n====== CRASH !!! ======");
			} else if (msgFromServer.msgType.equals("spawn")) {
				System.out.println("\n====== SPAWN FROM CRASH !!! ======");
			} else {
				send(new PingCommand());
			}
		}
	}

	boolean startToSlowDownForBend = false;

	public static final double[] safeVDistance = new double[]{-1, -1, 30, 70, 250, 300, 420, 600, 800};
	public static final double vSafeInBend = 6.4;
	public static final double[] safeVValue = new double[]{-1, vSafeInBend, 6.8, 7.5, 9, 10, 12, 15, Helper.INTEGER_INF};
	public static final double safeAngle = 40;

	/**
	 * return 0: 1
	 *
	 * @param v
	 * @param s
	 * @return
	 */
	public static double throttle(double v, double s, double angle, double radius) {
		v = v + Math.abs(angle / 180) - radius / 1000;
		int i = Arrays.binarySearch(safeVValue, v);
		if (i < 0) {
			i = -i - 1;
		}
		System.out.printf("----- v = %.2f s = %.2f angle = %.2f index = %d, throttle = %.2f", v, s, angle, i, safeVDistance[i] < s ? 1.0 : 0.0);
		if (angle > safeAngle) {
			return 0;
		}
		return safeVDistance[i] < s ? 1.0 : 0.01;
	}

	private void makeDecision() {
		int currPieceIndex = carPositionsData.data[0].piecePosition.pieceIndex;
		int nextPieceIndex = (currCarState.pieceIndex + 1) % gameState.nPieces;
		double inPieceDis = carPositionsData.data[0].piecePosition.inPieceDistance;
		boolean isStraightPiece = gameState.isStraightPieces[currPieceIndex];

		double distToBend = 0.0;
		if (isStraightPiece) {
			distToBend = gameState.distanceToNextBends[currPieceIndex] + gameState.piecesLengths[currCarState.laneIndex][currPieceIndex]
					- inPieceDis;
		} else {
			distToBend = 0;
		}
		currCarState.distanceToBend = distToBend;

		/**
		 * SWITCHING LANE *
		 */
		if (gameState.isSwitchablePieces[nextPieceIndex] && currCarState.switchingPieceIndex != currCarState.pieceIndex
				&& gameState.nextTurns[currCarState.pieceIndex] >= 0) {
			if (gameState.nextTurns[currCarState.pieceIndex] == GameState.NEXT_TURN_LEFT && currCarState.laneIndex > 0) {
				send(SwitchLaneCommand.LEFT);
				currCarState.switchingPieceIndex = currCarState.pieceIndex;
				return;
			}
			if (gameState.nextTurns[currCarState.pieceIndex] == GameState.NEXT_TURN_RIGHT && currCarState.laneIndex + 1 < gameState.nLanes) {
				send(SwitchLaneCommand.RIGHT);
				currCarState.switchingPieceIndex = currCarState.pieceIndex;
				return;
			}
		}

		/**
		 * TURBO *
		 */
		if (currCarState.throttle == 1.0 && isStraightPiece && gameState.shouldTurbos[currPieceIndex] && currCarState.turboAvail != null
				&& !currCarState.turboSending && currCarState.actualTurbo == null) {
			currCarState.turboSending = true;
			send(new TurboCommand());
			return;
		}

		/* CALCULATE THROTTLE */
		currCarState.throttle = throttle(currCarState.velocity, distToBend, currCarState.carAngle, gameState.piecesRadiuses[currPieceIndex]);
		/**
		 * THROTTLE *
		 */
		send(new ThrottleCommand(currCarState.throttle));
		/**
		 * **************************************************
		 */
	}

	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

//		String host = "webber.helloworldopen.com";
//		int port = 8091;
//		String botName = "Coc Coc - RobotVer2";
//		String botKey = "o8FWGE73+p5P1w";
		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		new RobotVer21(reader, writer, new JoinCommand(botName, botKey));
	}

	private void send(final AbstractCommand command) {
		System.out.println("\nSEND: " + command.toJson());
		writer.println(command.toJson());
		writer.flush();
	}
}

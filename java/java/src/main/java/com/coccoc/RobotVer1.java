package com.coccoc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.coccoc.entities.receive.CarPositionsData;
import com.coccoc.entities.receive.GameInitData;
import com.coccoc.entities.receive.YourCarData;
import com.coccoc.entities.send.AbstractCommand;
import com.coccoc.entities.send.JoinCommand;
import com.coccoc.entities.send.PingCommand;
import com.coccoc.entities.send.SwitchLaneCommand;
import com.coccoc.entities.send.ThrottleCommand;
import com.google.gson.Gson;

/**
 * The first version. Not using turbo yet.
 *
 * @author Coc Coc developers
 */
public class RobotVer1 {
	private static final Gson GSON = new Gson();
	private final PrintWriter writer;

	private static final double SAFETY_DISTANCE_FULL_SPEED = 300;
	private static final double DISTANCE_TO_REDUCE_SPEED = 250;

	private GameInitData gameInitData;
	private CarPositionsData carPositionsData;

	private final List<CarState> opponentStates;
	private final CarState prevCarState;
	private final CarState currCarState;
	private final GameState gameState;
	private YourCarData yourCar;

	public RobotVer1(final BufferedReader reader, final PrintWriter writer, final JoinCommand join) throws IOException {
		this.writer = writer;
		String line = null;

		send(join);

		opponentStates = new ArrayList<CarState>();
		prevCarState = new CarState();
		currCarState = new CarState();
		gameState = new GameState();

		while ((line = reader.readLine()) != null) {
			System.out.println("\nRECV: " + line);
			final MsgWrapper msgFromServer = GSON.fromJson(line, MsgWrapper.class);
			if (msgFromServer.msgType.equals("carPositions")) {
				carPositionsData = GSON.fromJson(line, CarPositionsData.class);
				Helper.calculateCarState(carPositionsData, gameState, yourCar, opponentStates, prevCarState, currCarState);
				makeDecision();
				System.out.println("STATE == " + CarState.toString(prevCarState, currCarState));
			} else if (msgFromServer.msgType.equals("join")) {
				System.out.println("\n====== JOINED ======");
			} else if (msgFromServer.msgType.equals("gameInit")) {
				System.out.println("\n====== RACE INIT ======");
				gameInitData = GSON.fromJson(line, GameInitData.class);
				Helper.initData(gameInitData, gameState, currCarState);
			} else if (msgFromServer.msgType.equals("gameEnd")) {
				System.out.println("\n====== RACE END ======");
			} else if (msgFromServer.msgType.equals("yourCar")) {
				System.out.println("\n====== YOUR CAR ======");
				yourCar = GSON.fromJson(line, YourCarData.class);
			}else if (msgFromServer.msgType.equals("gameStart")) {
				System.out.println("\n====== RACE START ======");
			} else if (msgFromServer.msgType.equals("crash")) {
				System.out.println("\n====== CRASH !!! ======");
			} else if (msgFromServer.msgType.equals("spawn")) {
				System.out.println("\n====== SPAWN FROM CRASH !!! ======");
			} else {
				send(new PingCommand());
			}
		}
	}

	private void makeDecision() {
		int currPieceIndex = carPositionsData.data[0].piecePosition.pieceIndex;
		int nextPieceIndex = (currCarState.pieceIndex + 1) % gameState.nPieces;
		double inPieceDis = carPositionsData.data[0].piecePosition.inPieceDistance;
		double distToBend = 0.0;
		if (gameState.isStraightPieces[currCarState.pieceIndex]) {
			distToBend = gameState.distanceToNextBends[currPieceIndex] + gameState.piecesLengths[currCarState.laneIndex][currPieceIndex] - inPieceDis;
		}

		// switching lane is possible
		if (gameState.isSwitchablePieces[nextPieceIndex] && currCarState.switchingPieceIndex != currCarState.pieceIndex && gameState.nextTurns[currCarState.pieceIndex] >= 0) {
			if (gameState.nextTurns[currCarState.pieceIndex] == GameState.NEXT_TURN_LEFT && currCarState.laneIndex > 0) {
				send(SwitchLaneCommand.LEFT);
				currCarState.switchingPieceIndex = currCarState.pieceIndex;
				return;
			}
			if (gameState.nextTurns[currCarState.pieceIndex] == GameState.NEXT_TURN_RIGHT && currCarState.laneIndex + 1 < gameState.nLanes) {
				send(SwitchLaneCommand.RIGHT);
				currCarState.switchingPieceIndex = currCarState.pieceIndex;
				return;
			}
		}

		// gas
		double speed = 1.0;
		if (gameState.hasBendPieces) {
			if (distToBend > SAFETY_DISTANCE_FULL_SPEED) {
				speed = 1.0;
			} else if (distToBend > DISTANCE_TO_REDUCE_SPEED) {
				speed = 0.75;
			} else {
				speed = 0.5;
			}
		}
		currCarState.throttle = speed;
		send(new ThrottleCommand(speed));
	}

	public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

		// String host = "webber.helloworldopen.com";
		// int port = 8091;
		// String botName = "Coc Coc - RobotVer1";
		// String botKey = "o8FWGE73+p5P1w";

		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		new RobotVer1(reader, writer, new JoinCommand(botName, botKey));
	}

	private void send(final AbstractCommand command) {
		System.out.println("\nSEND: " + command.toJson());
		writer.println(command.toJson());
		writer.flush();
	}
}

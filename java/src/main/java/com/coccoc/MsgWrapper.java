package com.coccoc;

import com.coccoc.entities.send.AbstractCommand;

public class MsgWrapper {
	public final String msgType;
	public final Object data;

	MsgWrapper(final String msgType, final Object data) {
		this.msgType = msgType;
		this.data = data;
	}

	public MsgWrapper(final AbstractCommand sendMsg) {
		this(sendMsg.msgType(), sendMsg.msgData());
	}
}

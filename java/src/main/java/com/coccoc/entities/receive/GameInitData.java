package com.coccoc.entities.receive;

import java.util.List;
import java.util.Map;

public class GameInitData {
	public String gameId;
	public Data data;
	public String msgType;

	public static class Data {
		public Race race;
	}

	public static class Race {
		public List<CarsEntry> cars;
		public Track track;
		public RaceSession raceSession;
	}

	public static class CarsEntry {
		public Map<String, String> id;
		public Map<String, Double> dimensions;
	}

	public static class Track {
		public String id;
		public List<Map<String, Object>> pieces;
		public StartingPoint startingPoint;
		public String name;
		public List<Lane> lanes;
	}
	
	public static class Lane {
		public int distanceFromCenter;
		public int index;
	}

	public static class StartingPoint {
		public Map<String, Double> position;
		public Double angle;
	}

	public static class RaceSession {
		// Qualifying
		public Integer durationMs;

		// Race
		public Integer laps;
		public Integer maxLapTimeMs;
		public Boolean quickRace;
	}
}
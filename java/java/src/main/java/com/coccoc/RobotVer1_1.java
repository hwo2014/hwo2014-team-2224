package com.coccoc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import com.coccoc.entities.receive.CarPositionsData;
import com.coccoc.entities.receive.GameInitData;
import com.coccoc.entities.receive.YourCarData;
import com.coccoc.entities.receive.turbo.TurboAvailableData;
import com.coccoc.entities.receive.turbo.TurboEndData;
import com.coccoc.entities.receive.turbo.TurboStartData;
import com.coccoc.entities.send.AbstractCommand;
import com.coccoc.entities.send.JoinCommand;
import com.coccoc.entities.send.PingCommand;
import com.coccoc.entities.send.ThrottleCommand;
import com.coccoc.entities.send.TurboCommand;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * The first version. Not using turbo yet.
 *
 * @author Coc Coc developers
 */
public class RobotVer1_1 {

	private static final Gson GSON = new Gson();
	private final PrintWriter writer;

	private static final double SAFETY_DISTANCE_FULL_SPEED = 100;
	private static final double DISTANCE_TO_REDUCE_SPEED = 50;

	private GameInitData gameInitData;
	private CarPositionsData carPositionsData;

	private List<CarState> opponentStates;
	private final CarState prevState;
	private final CarState currState;
	private final GameState gameState;
	private YourCarData yourCar;

	public RobotVer1_1(final BufferedReader reader, final PrintWriter writer, final JoinCommand join) throws IOException {
		this.writer = writer;
		String line = null;

		send(join);

		prevState = new CarState();
		currState = new CarState();
		gameState = new GameState();
		interact(reader);
	}

	public void interact(final BufferedReader reader) throws IOException, JsonSyntaxException {
		String line;
		while ((line = reader.readLine()) != null) {
			System.out.println("\nRECV: " + line);
			final MsgWrapper msgFromServer = GSON.fromJson(line, MsgWrapper.class);
			if (msgFromServer.msgType.equals("carPositions")) {
				carPositionsData = GSON.fromJson(line, CarPositionsData.class);
				Helper.calculateCarState(carPositionsData, gameState, yourCar, opponentStates, prevState, currState);
				makeDecision();
				System.out.println("STATE == " + CarState.toString(prevState, currState));
			} else if (msgFromServer.msgType.equals("join")) {
				System.out.println("Joined");
			} else if (msgFromServer.msgType.equals("gameInit")) {
				System.out.println("\n====== RACE INIT ======");
				gameInitData = GSON.fromJson(line, GameInitData.class);
				Helper.initData(gameInitData, gameState, currState);
			} else if (msgFromServer.msgType.equals("gameEnd")) {
				System.out.println("\n====== RACE END ======");
			} else if (msgFromServer.msgType.equals("yourCar")) {
				System.out.println("\n====== YOUR CAR ======");
				yourCar = GSON.fromJson(line, YourCarData.class);
			} else if (msgFromServer.msgType.equals("turboAvailable")) {
				System.out.println("\n====== TURBO AVAILABLE ======");
				currState.turboAvail = GSON.fromJson(line, TurboAvailableData.class);
			} else if (msgFromServer.msgType.equals("turboStart")) {
				System.out.println("\n====== TURBO START ======");
				TurboStartData turbo = GSON.fromJson(line, TurboStartData.class);
				if (yourCar.data.equals(turbo.data)) {
					currState.actualTurbo = currState.turboAvail;
					currState.turbo = true;
					currState.turboSending = prevState.turboSending = false;
				}
			} else if (msgFromServer.msgType.equals("turboEnd")) {
				System.out.println("\n====== TURBO END ======");
				TurboEndData turbo = GSON.fromJson(line, TurboEndData.class);
				if (yourCar.data.equals(turbo.data)) {
					currState.actualTurbo = null;
					currState.turbo = false;
				}
			} else if (msgFromServer.msgType.equals("gameStart")) {
				System.out.println("\n====== RACE START ======");
			} else if (msgFromServer.msgType.equals("crash")) {
				System.out.println("\n====== CRASH !!! ======");
			} else if (msgFromServer.msgType.equals("spawn")) {
				System.out.println("\n====== SPAWN FROM CRASH !!! ======");
			} else {
				send(new PingCommand());
			}
		}
	}

	private void makeDecision() {
		int currPieceIndex = carPositionsData.data[0].piecePosition.pieceIndex;
		int nextPieceIndex = (currPieceIndex + 1) % gameState.nPieces;
		double inPieceDis = carPositionsData.data[0].piecePosition.inPieceDistance;
		double distToBend = 0.0;
		boolean isStraightPiece = gameState.isStraightPieces[currPieceIndex];
		if (isStraightPiece) {
			distToBend = gameState.distanceToNextBends[currPieceIndex] + gameState.piecesLengths[currState.laneIndex][currPieceIndex] - inPieceDis;
		}
		double speed = 1.0;
		if (gameState.hasBendPieces) {
			if (distToBend > SAFETY_DISTANCE_FULL_SPEED) {
				speed = 1.0;
			} else if (distToBend > DISTANCE_TO_REDUCE_SPEED) {
				speed = 0;
			} else {
				speed = 0.5;
			}
		}
		currState.throttle = speed;
		currState.turboSending = prevState.turboSending;
		if (currState.throttle == 1.0 && isStraightPiece && distToBend > Helper.MIN_TURBO_DISTANCE && currState.turboAvail != null && !currState.turboSending) {
			//send turbo
			send(new TurboCommand());
		} else {
			send(new ThrottleCommand(speed));
		}
	}

	public static void main(String... args) throws IOException {
//        String host = args[0];
//        int port = Integer.parseInt(args[1]);
//        String botName = args[2];
//        String botKey = args[3];

		String host = "webber.helloworldopen.com";
		int port = 8091;
		String botName = "Coc Coc Version hai";
		String botKey = "o8FWGE73+p5P1w";

		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		new RobotVer1_1(reader, writer, new JoinCommand(botName, botKey));
	}

	private void send(final AbstractCommand command) {
		System.out.println("\nSEND: " + command.toJson());
		writer.println(command.toJson());
		writer.flush();
	}
}

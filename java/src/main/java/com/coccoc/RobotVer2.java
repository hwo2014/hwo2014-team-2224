package com.coccoc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.coccoc.entities.receive.CarPositionsData;
import com.coccoc.entities.receive.GameInitData;
import com.coccoc.entities.receive.LapFinishedData;
import com.coccoc.entities.receive.YourCarData;
import com.coccoc.entities.receive.turbo.TurboAvailableData;
import com.coccoc.entities.receive.turbo.TurboEndData;
import com.coccoc.entities.receive.turbo.TurboStartData;
import com.coccoc.entities.send.AbstractCommand;
import com.coccoc.entities.send.JoinCommand;
import com.coccoc.entities.send.PingCommand;
import com.coccoc.entities.send.SwitchLaneCommand;
import com.coccoc.entities.send.ThrottleCommand;
import com.coccoc.entities.send.TurboCommand;
import com.google.gson.Gson;

/**
 * The 2nd version. Simple turbo.
 *
 * @author Coc Coc developers
 */
public class RobotVer2 {
	private static final Gson GSON = new Gson();
	private final PrintWriter writer;

	private GameInitData gameInitData;
	private CarPositionsData carPositionsData;

	private final List<CarState> opponentStates;
	private final CarState prevCarState;
	private final CarState currCarState;
	private final GameState gameState;
	private YourCarData yourCar;

	public RobotVer2(final BufferedReader reader, final PrintWriter writer, final JoinCommand join) throws IOException {
		this.writer = writer;
		String line = null;

		send(join);

		opponentStates = new ArrayList<CarState>();
		prevCarState = new CarState();
		currCarState = new CarState();
		gameState = new GameState();

		while ((line = reader.readLine()) != null) {
			System.out.println("\nRECV: " + line);
			final MsgWrapper msgFromServer = GSON.fromJson(line, MsgWrapper.class);
			if (msgFromServer.msgType.equals("carPositions")) {
				carPositionsData = GSON.fromJson(line, CarPositionsData.class);
				Helper.calculateCarState(carPositionsData, gameState, yourCar, opponentStates, prevCarState, currCarState);
				makeDecision();
				System.out.println("\nSTAT: " + CarState.toString(prevCarState, currCarState));
			} else if (msgFromServer.msgType.equals("join")) {
				System.out.println("\n====== JOINED ======");
			} else if (msgFromServer.msgType.equals("gameInit")) {
				System.out.println("\n====== RACE INIT ======");
				gameInitData = GSON.fromJson(line, GameInitData.class);
				Helper.initData(gameInitData, gameState, currCarState);
				currCarState.pieceIndex = 0;
				prevCarState.pieceIndex = gameState.nPieces - 1;
			} else if (msgFromServer.msgType.equals("turboAvailable")) {
				System.out.println("\n====== TURBO AVAILABLE ======");
				currCarState.turboAvail = GSON.fromJson(line, TurboAvailableData.class);
			} else if (msgFromServer.msgType.equals("gameEnd")) {
				System.out.println("\n====== RACE END ======");
			} else if (msgFromServer.msgType.equals("lapFinished")) {
				LapFinishedData lapFinishedData = GSON.fromJson(line, LapFinishedData.class);
				System.out.println("\n====== LAP FINISHED ======");
				System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				System.out.println("LapTime = " + lapFinishedData.data.lapTime.millis + "\tRaceTime = " + lapFinishedData.data.raceTime.millis);
				System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				System.out.println("\n\n");
			} else if (msgFromServer.msgType.equals("yourCar")) {
				System.out.println("\n====== YOUR CAR ======");
				yourCar = GSON.fromJson(line, YourCarData.class);
			} else if (msgFromServer.msgType.equals("gameStart")) {
				System.out.println("\n====== RACE START ======");
			} else if (msgFromServer.msgType.equals("turboStart")) {
				System.out.println("\n====== TURBO START ======");
				TurboStartData turbo = GSON.fromJson(line, TurboStartData.class);
				if (yourCar.data.equals(turbo.data)) {
					currCarState.actualTurbo = currCarState.turboAvail;
					currCarState.turboAvail = null;
					currCarState.turbo = true;
					currCarState.turboSending = prevCarState.turboSending = false;
				}
			} else if (msgFromServer.msgType.equals("turboEnd")) {
				System.out.println("\n====== TURBO END ======");
				TurboEndData turbo = GSON.fromJson(line, TurboEndData.class);
				if (yourCar.data.equals(turbo.data)) {
					currCarState.actualTurbo = null;
					currCarState.turbo = false;
				}
			} else if (msgFromServer.msgType.equals("gameStart")) {
				System.out.println("\n====== RACE START ======");
			} else if (msgFromServer.msgType.equals("crash")) {
				System.out.println("\n====== CRASH !!! ======");
				System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
				System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
				System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
				System.out.println("\n\n");
			} else if (msgFromServer.msgType.equals("spawn")) {
				System.out.println("\n====== SPAWN FROM CRASH !!! ======");
			} else {
				send(new PingCommand());
			}
		}
	}

	boolean startToSlowDownForBend = false;

	private void makeDecision() {
		int currPieceIndex = carPositionsData.data[0].piecePosition.pieceIndex;
		int nextPieceIndex = (currCarState.pieceIndex + 1) % gameState.nPieces;
		double inPieceDis = carPositionsData.data[0].piecePosition.inPieceDistance;
		boolean isStraightPiece = gameState.isStraightPieces[currPieceIndex];
		double distToBend = 0.0;
		if (isStraightPiece) {
			distToBend = gameState.distanceToNextBends[currPieceIndex] + gameState.piecesLengths[currCarState.laneIndex][currPieceIndex]
					- inPieceDis;
		} else {
			distToBend = Math.max(0.0, gameState.piecesLengths[currCarState.laneIndex][currPieceIndex] - inPieceDis);
		}
		currCarState.distanceToBend = distToBend;
		double curveLevel = 0.0; // in range of [0.0, 1.0], lower is better
		if (isStraightPiece) {
			int nextCurveIndex = nextPieceIndex;
			while (gameState.isStraight(nextCurveIndex)) {
				nextCurveIndex = (nextCurveIndex + 1) % gameState.nPieces;
			}
			curveLevel = Helper.calculateCurveLevel(gameState.piecesRadiuses[nextCurveIndex], 0);
		} else {
			curveLevel = Helper.calculateCurveLevel(gameState.piecesRadiuses[currPieceIndex], currCarState.carAngle);
		}

		/*****************************************************/
		/** CALCULATING NEXT THROTTLE **/
		boolean isHardBend = !isStraightPiece && (gameState.piecesRadiuses[currPieceIndex] < Helper.RADIUS_THESH_FOR_DISTANCE_TO_BEND_BONUS);
		double throttleDelta = Helper.calculateThrottleDelta(currCarState, distToBend, !isStraightPiece, isHardBend, curveLevel);
		if (currCarState.actualTurbo != null) {
			if (distToBend >= Helper.SAFETY_DISTANCE_FULL_SPEED) {
				throttleDelta = 1.0;
			} else if (distToBend <= Helper.DISTANCE_TO_REDUCE_SPEED && throttleDelta < 0) {
				throttleDelta *= currCarState.actualTurbo.data.turboFactor;
			}
		} else {
			if (throttleDelta > 0) {
				throttleDelta *= 2;
			}
		}
		currCarState.throttle += throttleDelta;
		currCarState.throttle = Math.max(currCarState.throttle, 0.01);
		currCarState.throttle = Math.min(currCarState.throttle, isStraightPiece || !isHardBend ? 1.0 : 0.7);

		/*****************************************************/

		/*****************************************************/
		/** SWITCHING LANE **/
		if (gameState.isSwitchablePieces[nextPieceIndex] && currCarState.switchingPieceIndex != currCarState.pieceIndex
				&& gameState.nextTurns[currCarState.pieceIndex] >= 0) {
			if (gameState.nextTurns[currCarState.pieceIndex] == GameState.NEXT_TURN_LEFT && currCarState.laneIndex > 0) {
					send(SwitchLaneCommand.LEFT);
					currCarState.switchingPieceIndex = currCarState.pieceIndex;
					return;
			}
			if (gameState.nextTurns[currCarState.pieceIndex] == GameState.NEXT_TURN_RIGHT && currCarState.laneIndex + 1 < gameState.nLanes) {
					send(SwitchLaneCommand.RIGHT);
					currCarState.switchingPieceIndex = currCarState.pieceIndex;
					return;
			}
		}
		/*****************************************************/

		/*****************************************************/
		/** TURBO **/
//		if (currCarState.throttle == 1.0 && currCarState.velocity >= Helper.MIN_VELOCITY_TO_USE_TURBO
//				&& currCarState.acceleration >= Helper.MIN_ACCELERATION_TO_USE_TURBO && isStraightPiece
//				&& gameState.shouldTurbos[currPieceIndex] && distToBend > Helper.MIN_TURBO_DISTANCE && currCarState.turboAvail != null
//				&& !currCarState.turboSending && currCarState.actualTurbo == null) {
		if (currCarState.throttle == 1.0 && isStraightPiece && gameState.shouldTurbos[currPieceIndex]
				&& distToBend > Helper.MIN_TURBO_DISTANCE && currCarState.turboAvail != null && !currCarState.turboSending
				&& currCarState.actualTurbo == null) {
			System.out.println("\n====== SENDING TURBO ======");
			System.out.println("========================>>>>>");
			System.out.println("========================>>>>>");
			System.out.println("========================>>>>>");
			System.out.println("\n\n");
			currCarState.turboSending = true;
			send(new TurboCommand());
			return;
		}
		/*****************************************************/

		/*****************************************************/
		/** THROTTLE **/
		send(new ThrottleCommand(currCarState.throttle));
		/*****************************************************/
	}

	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

//		String host = "webber.helloworldopen.com";
//		int port = 8091;
//		String botName = "Coc Coc - RobotVer2";
//		String botKey = "o8FWGE73+p5P1w";

		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		new RobotVer2(reader, writer, new JoinCommand(botName, botKey));
	}

	private void send(final AbstractCommand command) {
		System.out.println("\nSEND: " + command.toJson());
		writer.println(command.toJson());
		writer.flush();
	}
}

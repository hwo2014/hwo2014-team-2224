package com.coccoc;

import java.util.List;
import java.util.Map;

import com.coccoc.entities.receive.CarPositionsData;
import com.coccoc.entities.receive.CarPositionsData.DataEntry;
import com.coccoc.entities.receive.GameInitData;
import com.coccoc.entities.receive.YourCarData;

/**
 * Helper class.
 *
 * @author Coc Coc developers
 */
public class Helper {
	public static final int INTEGER_INF = 1000000000;
	public static final int INTEGER_NA = -1;
	public static final int TURBO_PIECE_COUNT = 8;
	public static final double DOUBLE_NA = -1.0;
	public static final double DOUBLE_ZERO = 0.0;
	public static final int MIN_TURBO_DISTANCE = 250;
	private static final double SAFETY_VELOCITY_HARD_BEND = 6.3; // for curve which radius < 150
	private static final double SAFETY_VELOCITY_MIN = 6.7; // for curve level 1.0
	private static final double SAFETY_VELOCITY_MAX = 14; // for curve level 0.0
	public static final double RADIUS_THESH_FOR_DISTANCE_TO_BEND_BONUS = 150;
	public static final double MIN_VELOCITY_TO_USE_TURBO = 5.0;
	public static final double MIN_ACCELERATION_TO_USE_TURBO = 0.05;

	private Helper() {
	}

	public static void initData(GameInitData gameInitData, GameState gameState, CarState carState) {
		gameState.isQualificationRound = gameInitData.data.race.raceSession.durationMs != null;
		gameState.nLaps = (gameState.isQualificationRound) ? 0 : gameInitData.data.race.raceSession.laps;
		gameState.nLanes = gameInitData.data.race.track.lanes.size();
		gameState.nPieces = gameInitData.data.race.track.pieces.size();
		gameState.isStraightPieces = new boolean[gameState.nPieces];
		gameState.isSwitchablePieces = new boolean[gameState.nPieces];
		gameState.piecesAngles = new double[gameState.nPieces];
		gameState.piecesRadiuses = new double[gameState.nPieces];
		gameState.piecesLengths = new double[gameState.nLanes][gameState.nPieces];
		gameState.distanceToNextBends = new double[gameState.nPieces];
		gameState.shouldTurbos = new boolean[gameState.nPieces];
		gameState.nextTurns = new int[gameState.nPieces];
		gameState.hasBendPieces = false;

		System.out.println("Initializing lengths");
		for (int i = 0; i < gameState.nPieces; i++) {
			Map<String, Object> p = gameInitData.data.race.track.pieces.get(i);
			gameState.isStraightPieces[i] = p.containsKey("length");
			gameState.hasBendPieces |= !gameState.isStraightPieces[i];
			gameState.isSwitchablePieces[i] = p.containsKey("switch") && Boolean.TRUE.equals(p.get("switch"));
			gameState.piecesAngles[i] = gameState.isStraightPieces[i] ? -1 : Double.parseDouble(p.get("angle").toString());
			gameState.piecesRadiuses[i] = gameState.isStraightPieces[i] ? -1 : Double.parseDouble(p.get("radius").toString());

			if (gameState.isStraightPieces[i]) {
				for (int j = 0; j < gameState.nLanes; ++j) {
					gameState.piecesLengths[j][i] = Double.parseDouble(p.get("length").toString());
				}
			} else {
				for (int j = 0; j < gameState.nLanes; ++j) {
					gameState.piecesLengths[gameInitData.data.race.track.lanes.get(j).index][i] = (gameState.piecesRadiuses[i] - gameInitData.data.race.track.lanes
							.get(j).distanceFromCenter * Math.signum(gameState.piecesAngles[i]))
							* Math.abs(gameState.piecesAngles[i] / 180.0 * Math.PI);
				}
			}
		}
		System.out.println("Initializing distance to next bends");
		if (gameState.hasBendPieces) {
			int flag = 0;
			while (gameState.isStraightPieces[flag]) {
				flag = (gameState.nPieces + flag - 1) % gameState.nPieces;
			}
			gameState.distanceToNextBends[flag] = (gameState.piecesRadiuses[flag] > RADIUS_THESH_FOR_DISTANCE_TO_BEND_BONUS) ? gameState.piecesLengths[0][flag] : 0;

			int index = (gameState.nPieces + flag - 1) % gameState.nPieces;
			while (index != flag) {
				int nextIndex = (index + 1) % gameState.nPieces;
				if (gameState.isStraightPieces[index]) {
					gameState.distanceToNextBends[index] = gameState.piecesLengths[0][index];
					if (gameState.isStraightPieces[nextIndex]) {
						gameState.distanceToNextBends[index] += gameState.distanceToNextBends[nextIndex];
					} else {
						gameState.distanceToNextBends[index] += gameState.piecesLengths[0][nextIndex];
					}
				} else {
					if (gameState.piecesRadiuses[index] <= RADIUS_THESH_FOR_DISTANCE_TO_BEND_BONUS) {
						gameState.distanceToNextBends[index] = 0;
					} else {
						gameState.distanceToNextBends[index] = gameState.piecesLengths[0][index];
						if (gameState.isStraightPieces[nextIndex]) {
							gameState.distanceToNextBends[index] += gameState.distanceToNextBends[nextIndex];
						}
					}
				}
				index = (gameState.nPieces + index - 1) % gameState.nPieces;
			}
		}
		for (int i = 0; i < TURBO_PIECE_COUNT; ++i) {
			double bestVal = -1;
			int bestIndex = -1;
			for (int j = 0; j < gameState.nPieces; ++j) {
				if (!gameState.shouldTurbos[j] && gameState.distanceToNextBends[j] > bestVal) {
					bestVal = gameState.distanceToNextBends[j];
					bestIndex = j;
				};
			}
			if (bestVal > 0) {
				gameState.shouldTurbos[bestIndex] = true;
			} else {
				break;
			}
		}
		System.out.println("Initializing turn suggesttion");
		for (int i = 0; i < gameState.nPieces; i++) {
			gameState.nextTurns[i] = -1;
			for (int nextI = (i + 1) % gameState.nPieces; nextI != i; nextI = (nextI + 1) % gameState.nPieces) {
				if (!gameState.isStraightPieces[nextI]) {
					gameState.nextTurns[i] = (gameState.piecesAngles[nextI] > 0.0) ? GameState.NEXT_TURN_RIGHT : GameState.NEXT_TURN_LEFT;
					break;
				}
			}
		}
		System.out.println("INIT STATE === " + gameState);
	}

	public static void calculateCarState(CarPositionsData carPositionsData, GameState gameState, YourCarData yourCar,
			List<CarState> opponentStates, CarState prevState, CarState currState) {
		CarState.copy(currState, prevState);
		opponentStates.clear();

		for (DataEntry dataEntry : carPositionsData.data) {
			if (dataEntry.id.equals(yourCar.data)) {
				parseIndividualCar(dataEntry, currState);
			} else {
				CarState opp = new CarState();
				parseIndividualCar(dataEntry, opp);
				opponentStates.add(opp);
			}
		}

		currState.radius = gameState.piecesRadiuses[currState.pieceIndex];
		currState.velocity = (prevState == null || prevState.laneIndex == Helper.INTEGER_NA) ? 0
				: (prevState.pieceIndex == currState.pieceIndex ? currState.inPieceDistance - prevState.inPieceDistance
				: gameState.piecesLengths[prevState.laneIndex][prevState.pieceIndex] - prevState.inPieceDistance
				+ currState.inPieceDistance);
		if (currState.velocity < -1e-9) {
			currState.velocity = prevState.velocity;
		}
		currState.acceleration = (prevState == null || prevState.laneIndex == Helper.INTEGER_NA) ? 0
				: (currState.velocity - prevState.velocity);
		if (currState.switchingPieceIndex != currState.pieceIndex) {
			currState.switchingPieceIndex = -1;
		}
	}

	private static void parseIndividualCar(DataEntry dataEntry, CarState carState) {
		carState.carAngle = dataEntry.angle;
		carState.inPieceDistance = dataEntry.piecePosition.inPieceDistance;
		carState.laneIndex = dataEntry.piecePosition.lane.endLaneIndex;
		carState.pieceIndex = dataEntry.piecePosition.pieceIndex;
	}

	/**
	 * Delta(Throttle) = acceleration * 4
	 */
	public static double calculateThrottleDelta(CarState carState, double distToBend, boolean isOnBend, boolean isHardBend, double curveLevel) {
		double v0 = carState.velocity;
		double v1 = 0.0;
		if (isOnBend) {
			v1 = (isHardBend) ? SAFETY_VELOCITY_HARD_BEND : SAFETY_VELOCITY_MIN;
		} else {
			v1 = SAFETY_VELOCITY_MIN + (SAFETY_VELOCITY_MAX - SAFETY_VELOCITY_MIN) * (1.0 - curveLevel);
		}
		double deltaV = v1 - v0; // a * t
		double S = distToBend; // S = v0 * t + (a * t * t * 0.5) = t * (v0 + deltaV * 0.5)
		double t = S / (v0 + deltaV * 0.5);
		double a = deltaV / t;
		double throttleDelta = a * 4.0;
		System.out.println(String.format("\nThrottleDelta: %.2f\tV: %.2f - > %.2f\tA: %.2f\tt: %.2f\tS: %.2f\tcurveLevel: %.2f", throttleDelta, v0, v1, a, t, S, curveLevel));
		return throttleDelta;
	}

	public static double calculateCurveLevel(double radius, double carAngle) {
		double level = 0.0;
		if (radius < 300) {
			level = 1.0 - radius / 300.0;
		}
		level = Math.max(1.0, level + Math.min(90.0, Math.abs(carAngle)) * 0.0005);
		return level;
	}

	public static double calculateThrottleManualTuned(GameState gameState, CarState currCarState, CarState prevCarState, int currPieceIndex, boolean isStraightPiece, double distToBend) {
		/*
		 * Idea of choosing throttle: to keep velocity on edge always < 6.5 (for this map)
		 * base on curve level (angle), we can compute maximum velocity and choose suitable throttle for each velocity
		 * example for this map, in each bend we can choose some good pair:
		 * (v, t) = (~6.4, 5.6), (~7.0, 0.2), (0.1, 7.5), (0.05, 8.0), (6.0, 8.5), (5.0, 9.5)
		 */
		double throttle = currCarState.throttle;
		if (!gameState.hasBendPieces) {
			throttle = 1.0;
		} else if (isStraightPiece) {
			if (distToBend >= Helper.MIN_TURBO_DISTANCE / 4 || currCarState.velocity < 5.5) {
				throttle = 1.0;
			} else if (currCarState.velocity < 6) {
				throttle = 0.9;
			}
			if (distToBend < Helper.MIN_TURBO_DISTANCE / 4 && currCarState.velocity > 8.5) {
				throttle = 0.01;
			}
		} else {
			boolean enterBend = gameState.isStraight(prevCarState.pieceIndex);
			if (enterBend) {
				throttle = Math.min(throttle / 2.5, 0.2);
				if (currCarState.velocity > 8.5) {
					throttle = 0.01;
				} else if (currCarState.velocity > 8) {
					throttle = 0.1;
				} else if (currCarState.velocity > 7) {
					throttle = 0.13;
				} else if (currCarState.velocity < 6.0) {
					throttle = 0.8;
				} else if (currCarState.velocity >= 6.5) {
					throttle -= 0.1;
				} else {
					throttle = 0.5;
				}
			} else if (Math.abs(currCarState.carAngle) > 180 - Math.abs(gameState.piecesAngles[currPieceIndex])) {
				throttle = 1.0;
			} else {
				throttle = Math.max(throttle / 1.8, 0.55);
				if (currCarState.velocity < 5.0) {
					throttle += 0.4;
				} else if (currCarState.velocity < 5.5) {
					throttle += 0.35;
				} else if (currCarState.velocity < 6.0) {
					throttle += 0.25;
				} else if (currCarState.velocity > 7.99) {
					throttle -= 0.5;
				} else if (currCarState.velocity > 7.49) {
					throttle -= 0.45;
				} else if (currCarState.velocity > 6.99) {
					throttle -= 0.4;
				} else if (currCarState.velocity > 6.69) {
					throttle -= 0.35;
				} else if (currCarState.velocity > 6.5) {
					throttle -= 0.3;
				} else if (currCarState.velocity > 6.4) {
					throttle -= 0.25;
				}
				if (throttle >= 0.99) {
					throttle = 1.0;
				}
				if (throttle <= 0.01) {
					throttle = 0.01;
				}
			}
		}
		return throttle;
	}
}

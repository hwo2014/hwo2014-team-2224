package com.coccoc.entities.receive;

import java.util.List;

public class GameEndData {
	public String gameId;
	public Data data;
	public String msgType;

	public static class Data {
		public List<ResultsEntry> results;
		public List<BestLapsEntry> bestLaps;
	}

	public static class ResultsEntry {
		public CarData car;
		public ResultData result;
	}

	public static class BestLapsEntry {
		public CarData car;
		public ResultData result;
	}
}

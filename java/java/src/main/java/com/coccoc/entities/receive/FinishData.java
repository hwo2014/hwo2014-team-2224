package com.coccoc.entities.receive;

public class FinishData {
	public String gameId;
	public Object data;
	public Integer gameTick;
	public String msgType;
}

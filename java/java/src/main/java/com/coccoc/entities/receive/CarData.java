package com.coccoc.entities.receive;

public class CarData {
	public String name;
	public String color;

	@Override
	public boolean equals(Object obj) {
		CarData c = (CarData)obj;
		return name.equals(c.name) && color.equals(c.color);
	}
}

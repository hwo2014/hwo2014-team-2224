package com.coccoc.entities.receive.turbo;

import com.coccoc.entities.receive.CarData;

public class TurboStartData {
	public CarData data;
	public Integer gameTick;
	public String msgType;
}

package com.coccoc.entities.send;

public class ThrottleCommand extends AbstractCommand {
	private final double value; // in range of [0.0, 1.0]

	public ThrottleCommand(double value) {
		this.value = value;
	}

	@Override
	public Object msgData() {
		return value;
	}

	@Override
	public String msgType() {
		return "throttle";
	}
}

package com.coccoc.entities.receive.crash;

import com.coccoc.entities.receive.CarData;

public class CrashData {
	public String gameId;
	public CarData data;
	public Integer gameTick;
	public String msgType;
}

package com.coccoc.entities.receive.turbo;

public class TurboAvailableData {
	public String gameId;
	public Data data;
	public Integer gameTick;
	public String msgType;

	public static class Data {
		public Double turboFactor;
		public Double turboDurationMilliseconds;
		public Integer turboDurationTicks;
	}
}

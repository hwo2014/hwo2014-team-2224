package com.coccoc.entities.receive.crash;

import com.coccoc.entities.receive.CarData;

public class DnfData {
	public String gameId;
	public CarData data;
	public String reason;
	public String msgType;
}

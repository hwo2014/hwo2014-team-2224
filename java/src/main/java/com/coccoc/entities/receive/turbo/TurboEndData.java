package com.coccoc.entities.receive.turbo;

import com.coccoc.entities.receive.CarData;

public class TurboEndData {
	public CarData data;
	public Integer gameTick;
	public String msgType;
}

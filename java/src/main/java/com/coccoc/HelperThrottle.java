package com.coccoc;

import java.util.TreeMap;

/**
 *
 * @author tunght
 */
public class HelperThrottle {

	private static final double DIS_TO_REDUCE_V = 230;

	/*
	 * In curve
	 */
	private static final TreeMap<Double, Double> CURVE_2_V = new TreeMap<>();
	private static final TreeMap<Double, Double> IN_CUREVE_V_2_THROTTLE = new TreeMap<>();

	static {
		// curve level to max v
		CURVE_2_V.put(10.01 / 180, 100.0);
		CURVE_2_V.put(22.6 / 180, 8.0);
		CURVE_2_V.put(30.1 / 180, 7.5);
		CURVE_2_V.put(35.1 / 180, 7.2);
		CURVE_2_V.put(40.1 / 180, 7.0);
		CURVE_2_V.put(45.1 / 180, 6.5);
		CURVE_2_V.put(50.1 / 180, 6.2);
		CURVE_2_V.put(70.1 / 180, 6.0);
		CURVE_2_V.put(90.1 / 180, 5.5);
		CURVE_2_V.put(135.1 / 180, 5.0);
		CURVE_2_V.put(180.1 / 180, 4.5);
		// in curve velocity 2 throttle
		IN_CUREVE_V_2_THROTTLE.put(4.51, 1.0);
		IN_CUREVE_V_2_THROTTLE.put(5.01, 0.95);
		IN_CUREVE_V_2_THROTTLE.put(5.51, 0.9);
		IN_CUREVE_V_2_THROTTLE.put(6.01, 0.8);
		IN_CUREVE_V_2_THROTTLE.put(6.21, 0.6);
		IN_CUREVE_V_2_THROTTLE.put(6.31, 0.55);
		IN_CUREVE_V_2_THROTTLE.put(6.41, 0.5);
		IN_CUREVE_V_2_THROTTLE.put(6.51, 0.45);
		IN_CUREVE_V_2_THROTTLE.put(6.71, 0.4);
		IN_CUREVE_V_2_THROTTLE.put(7.01, 0.25);
		IN_CUREVE_V_2_THROTTLE.put(7.31, 0.2);
		IN_CUREVE_V_2_THROTTLE.put(7.51, 0.3);
		IN_CUREVE_V_2_THROTTLE.put(Double.MAX_VALUE, 0.0);
	}

	private static final TreeMap<Double, Double> DELTA_V_2_DELTA_THR = new TreeMap<>();
	private static final TreeMap<Double, Double> THR_2_DELTA_V = new TreeMap<>();

	static {
		DELTA_V_2_DELTA_THR.put(0.3, 0.1);
		DELTA_V_2_DELTA_THR.put(0.5, 0.15);
		DELTA_V_2_DELTA_THR.put(0.7, 0.2);
		DELTA_V_2_DELTA_THR.put(1.5, 0.3);
		DELTA_V_2_DELTA_THR.put(2.0, 0.35);
		DELTA_V_2_DELTA_THR.put(4.0, 0.4);
		DELTA_V_2_DELTA_THR.put(Double.MAX_VALUE, 1.0);
		// throttle to delta v
		THR_2_DELTA_V.put(0.1, 0.05);
		THR_2_DELTA_V.put(0.2, 0.06);
		THR_2_DELTA_V.put(0.3, 0.08);
		THR_2_DELTA_V.put(0.4, 0.1);
		THR_2_DELTA_V.put(0.5, 0.12);
		THR_2_DELTA_V.put(0.6, 0.13);
		THR_2_DELTA_V.put(0.7, 0.15);
		THR_2_DELTA_V.put(0.8, 0.14);
		THR_2_DELTA_V.put(0.9, 0.18);
		THR_2_DELTA_V.put(1.1, 0.2);
	}

	public static double calculateThrottle(GameState gameState, CarState currCarState, CarState prevCarState, double distToBend,
		int nextPieceIndex, boolean isStraightPiece, int currPieceIndex) {
		double curveLevel = Math.abs(gameState.piecesAngles[currPieceIndex]) / 180.0; // in range of [0.0, 1.0], lower is better
		double maxV = CURVE_2_V.get(CURVE_2_V.ceilingKey(curveLevel));
		double deltaV;
		double deltaThrottle;
		double throttle;
		if (isStraightPiece) {
			int nextCurvePieceIndex = gameState.nextBendId[currPieceIndex];
			curveLevel = Math.abs(gameState.piecesAngles[nextCurvePieceIndex]) / 180; // in range of [0.0, 1.0], lower is better
			maxV = CURVE_2_V.get(CURVE_2_V.ceilingKey(curveLevel));
			double a = THR_2_DELTA_V.get(THR_2_DELTA_V.ceilingKey(prevCarState.throttle));
			deltaV = maxV - currCarState.velocity;
			double maxDisToReduce = 30 + deltaV * deltaV / (Math.abs(a));
			if (distToBend > Math.max(maxDisToReduce, DIS_TO_REDUCE_V)) {
				deltaThrottle = 1.0;
			} else if (!currCarState.turbo) {
				deltaThrottle = DELTA_V_2_DELTA_THR.get(DELTA_V_2_DELTA_THR.ceilingKey(Math.abs(deltaV)));
				if (deltaV < 0.0) {
					deltaThrottle = -deltaThrottle;
				}
			} else {
				deltaThrottle = 1.0;
			}
		} else {
			deltaThrottle = IN_CUREVE_V_2_THROTTLE.get(IN_CUREVE_V_2_THROTTLE.ceilingKey(maxV)) - prevCarState.throttle;
			if (currCarState.velocity > maxV) {
				deltaThrottle = -deltaThrottle;
			}
		}
		throttle = prevCarState.throttle + deltaThrottle;
		throttle = Math.min(1.0, Math.max(throttle, 0.0));
		if (!isStraightPiece) {
			boolean enterBend = gameState.isStraight(prevCarState.pieceIndex);
			if (enterBend) {
				if (currCarState.velocity > 8.5) {
					throttle = 0.05;
				} else if (currCarState.velocity > 8) {
					throttle = 0.15;
				} else if (currCarState.velocity > 7) {
					throttle = 0.2;
				} else if (currCarState.velocity < 6.0) {
					throttle = 0.85;
				} else if (currCarState.velocity >= maxV) {
					throttle -= 0.1;
				} else {
					throttle = 0.5;
				}
			} else if (Math.abs(currCarState.carAngle) > 180 - Math.abs(gameState.piecesAngles[currPieceIndex])) {
				throttle = 1.0;
			} else {
				throttle = Math.max(throttle / 1.8, 0.5);
				if (currCarState.velocity < 5.0) {
					throttle += 0.4;
				} else if (currCarState.velocity < 5.5) {
					throttle += 0.35;
				} else if (currCarState.velocity < 6.0) {
					throttle += 0.25;
				} else if (currCarState.velocity < 6.31) {
					throttle += 0.15;
				} else if (currCarState.velocity > 7.99) {
					throttle -= 0.5;
				} else if (currCarState.velocity > 7.49) {
					throttle -= 0.45;
				} else if (currCarState.velocity > 6.99) {
					throttle -= 0.4;
				} else if (currCarState.velocity > 6.69) {
					throttle -= 0.35;
				} else if (currCarState.velocity > 6.5) {
					throttle -= 0.3;
				} else if (currCarState.velocity > 6.4) {
					throttle -= 0.25;
				}
			}
			if (gameState.isStraight(nextPieceIndex)
				&& currCarState.inPieceDistance >= 0.7 * gameState.piecesLengths[currCarState.laneIndex][currPieceIndex]) {
				throttle = 1.0;
			}
			if (throttle >= 0.99) {
				throttle = 1.0;
			}
			if (throttle <= 0.01) {
				throttle = 0.01;
			}
		}
		return throttle;
	}

}

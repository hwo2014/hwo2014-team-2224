package com.coccoc.entities.receive;

public class ResultData {
	public Integer lap; // for BestLapsEntry, lapTime
	public Integer laps; // for ResultsEntry, raceTime
	public Integer ticks;
	public Integer millis;
}
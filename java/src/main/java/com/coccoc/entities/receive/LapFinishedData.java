package com.coccoc.entities.receive;

public class LapFinishedData {
	public String gameId;
	public Data data;
	public Integer gameTick;
	public String msgType;

	public static class Data {
		public CarData car;
		public ResultData raceTime;
		public ResultData lapTime;
		public Ranking ranking;
	}

	public static class Ranking {
		public Integer fastestLap;
		public Integer overall;
	}
}

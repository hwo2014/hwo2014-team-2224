package com.coccoc.entities.send;


public class SwitchLaneCommand extends AbstractCommand {
	public static final SwitchLaneCommand LEFT = new SwitchLaneCommand("Left");
	public static final SwitchLaneCommand RIGHT = new SwitchLaneCommand("Right");

	public String data; // Left or Right

	private SwitchLaneCommand(String data) {
		this.data = data;
	}

	@Override
	public Object msgData() {
		return data;
	}

	@Override
	public String msgType() {
		return "switchLane";
	}
}
